package com.kenyo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DempApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DempApiApplication.class, args);
	}

}
