package com.kenyo.demo.models.response;

import java.util.List;
import java.util.stream.Collectors;

import com.kenyo.demo.models.ERol;
import com.kenyo.demo.models.Rol;
import com.kenyo.demo.models.Usuario;

public class JwtResponse {
	
	private Usuario usuario;
	private String accessToken;
	private String tokenType;
	
	public JwtResponse() {}
	
	public JwtResponse(String jwt, long userId, String username, String email, List<String> roles) {
		this.accessToken = jwt;
		this.usuario = new Usuario();
		this.usuario.setId(userId);
		this.usuario.setUsername(username);
		this.usuario.setEmail(email);
		List<Rol> _roles = roles.stream().map(r -> {
			Rol rol = new Rol();
			if(ERol.ROLE_ADMIN.name().equals(r))
				rol.setName(ERol.ROLE_ADMIN);
			if(ERol.ROLE_USER.name().equals(r))
				rol.setName(ERol.ROLE_USER);
			if(ERol.ROLE_OTHER.name().equals(r))
				rol.setName(ERol.ROLE_OTHER);
			return rol;
		}).collect(Collectors.toList());
		this.usuario.setRoles(_roles);
		this.tokenType = "Bearer";
	}
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	
	

}
