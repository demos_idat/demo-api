package com.kenyo.demo.models;

public enum ERol {
	ROLE_USER,
    ROLE_OTHER,
    ROLE_ADMIN
}
