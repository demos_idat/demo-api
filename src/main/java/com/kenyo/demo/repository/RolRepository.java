package com.kenyo.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kenyo.demo.models.ERol;
import com.kenyo.demo.models.Rol;

public interface RolRepository extends JpaRepository<Rol, Long>{

	Optional<Rol> findByName(ERol name);
	
}
