package com.kenyo.demo.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kenyo.demo.models.ERol;
import com.kenyo.demo.models.Rol;
import com.kenyo.demo.models.Usuario;
import com.kenyo.demo.models.request.LoginRequest;
import com.kenyo.demo.models.request.SignupRequest;
import com.kenyo.demo.models.response.JwtResponse;
import com.kenyo.demo.models.response.MessageResponse;
import com.kenyo.demo.repository.RolRepository;
import com.kenyo.demo.repository.UsuarioRepository;
import com.kenyo.demo.security.jwt.JwtUtils;
import com.kenyo.demo.service.UserDetailsImpl;

@Controller
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	RolRepository rolRepository;

	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	JwtUtils jwtUtils;
	
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (usuarioRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username ya esta siendo usado!"));
		}

		if (usuarioRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email ya está siendo usado!"));
		}

		// Create new user's account
		Usuario user = new Usuario(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		List<String> srtRoles =  signUpRequest.getRoles();
		List<Rol> roles = new ArrayList<Rol>();
		
		

		if (srtRoles == null) {
			System.out.println("No hay roles");
			Rol userRole = rolRepository.findByName(ERol.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role no se encuentra."));
			roles.add(userRole);
		} else {
			srtRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Rol adminRole = rolRepository.findByName(ERol.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Rol no encontrado."));
					roles.add(adminRole);

					break;
				case "other":
					Rol otherRole = rolRepository.findByName(ERol.ROLE_OTHER)
							.orElseThrow(() -> new RuntimeException("Error: Rol no encontrado."));
					roles.add(otherRole );

					break;
				default:
					Rol userRole = rolRepository.findByName(ERol.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role no encontrado."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		usuarioRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("Usuario registrado correctamente!"));
	}


}
